UNIT WishList;


INTERFACE

TYPE
  WishNodePtr = ^WishNode;
  WishNode = RECORD
    pred, next: WishNodePtr;
    wish: STRING;
    n: INTEGER;
  END;

  (* Acts as anchor *)
  WishListPtr = WishNodePtr;

  WLInserter = PROCEDURE(wlp: WishListPtr; wish: String);


(* inserts wish lexicographically *)
PROCEDURE InsertAlphabetically(wlp: WishListPtr; wish: String);

(* prepends (is called 'insertfront' for     *)
(* consistency with other insertion methods) *)
PROCEDURE InsertFront(wlp: WishListPtr; wish: String);

(* appends (following the naming convention as well)*)
PROCEDURE InsertBack(wlp: WishListPtr; wish: String);

(* inserts wish using the inserter procedure *)
FUNCTION WishListFromFile(path: String; inserter: WLInserter): WishListPtr;

(* prints all list contents to stdout *)
PROCEDURE WriteList(wlp: WishListPtr);

(* responsible for disposal (returns true if deletion was successful )*)
FUNCTION DisposeWishList(wlp: WishListPtr): BOOLEAN;


IMPLEMENTATION

(* private utility function *)
(* creates wish list node on heap *)
FUNCTION  CreateWishNode(wish: String): WishNodePtr;
VAR wnp: WishNodePtr;
BEGIN
  New(wnp);
  IF wish = '' THEN BEGIN
    (* anchor element *)
    wnp^.n := -1;
  END ELSE BEGIN
    (* new wish *)
    wnp^.n := 1;
  END;
  (* also assign '' because why not lol *)
  wnp^.wish := wish;
  wnp^.next := wnp;
  wnp^.pred := wnp;
  CreateWishNode := wnp;
END;

(* private utility function *)
PROCEDURE InsertAfter(after, to_insert: WishNodePtr);
BEGIN
  IF (after <> nil) AND (to_insert <> nil) THEN BEGIN
    (* link <to_insert> and the previous successor of <after> *)
    to_insert^.next := after^.next;
    to_insert^.next^.pred := to_insert;
    (* link <to_insert> and <after> *)
    to_insert^.pred := after;
    after^.next := to_insert;
  END;
END;

PROCEDURE InsertAlphabetically(wlp: WishListPtr; wish: String);
VAR node, after: WishNodePtr;
BEGIN
  node := nil;
  IF (wlp <> nil) AND (wish <> '') THEN BEGIN

    after := wlp^.next;
    WHILE (after <> wlp) AND (after^.wish < wish) DO
      (* after^.next is guaranteed to not be nil *)
      (* due to implementation of InsertAfter and CreateWishNode *)
      after := after^.next;

    IF (after = wlp) OR (after^.wish > wish) THEN BEGIN
      node := CreateWishNode(wish);
      InsertAfter(after^.pred, node);
    END ELSE IF after^.wish = wish THEN
      after^.n += 1;

  END;
END;

PROCEDURE InsertFront(wlp: WishListPtr; wish: String);
VAR node, buffer: WishNodePtr;
BEGIN
  node := nil;
  IF (wlp <> nil) AND (wish <> '') THEN BEGIN

    buffer := wlp^.next;
    WHILE (buffer <> wlp) AND (buffer^.wish <> wish) DO
      (* buffer^.next is guaranteed to not be nil *)
      (* due to implementation of InsertAfter and CreateWishNode *)
      buffer := buffer^.next;

    IF buffer = wlp THEN BEGIN
      node := CreateWishNode(wish);
      InsertAfter(wlp, node);
    END ELSE BEGIN (* wishes must have matched *)
      buffer^.n += 1;
      (* link predecessor and successor of <buffer> *)
      buffer^.pred^.next := buffer^.next;
      buffer^.next^.pred := buffer^.pred;
      InsertAfter(wlp, buffer);
    END;

  END;
END;

PROCEDURE InsertBack(wlp: WishListPtr; wish: String);
VAR node, after: WishNodePtr;
BEGIN
  node := nil;
  IF (wlp <> nil) AND (wish <> '') THEN BEGIN

    after := wlp^.next;
    WHILE (after <> wlp) AND (after^.wish <> wish) DO
      (* after^.next is guaranteed to not be nil *)
      (* due to implementation of InsertAfter and CreateWishNode *)
      after := after^.next;

    IF after = wlp THEN BEGIN
      node := CreateWishNode(wish);
      InsertAfter(wlp^.pred, node);
    END ELSE BEGIN (* wishes must have matched *)
      after^.n += 1;
    END;

  END;
END;

FUNCTION WishListFromFile(path: String; inserter: WLInserter): WishListPtr;
VAR
  wish_file: TEXT;
  wish: String;
  wlp: WishListPtr;
BEGIN
  Assign(wish_file, path);
  Reset(wish_file);
  (* create anchor *)
  wlp := CreateWishNode('');
  REPEAT
    ReadLn(wish_file, wish);
    inserter(wlp, wish);
  UNTIL Eof(wish_file);
  Close(wish_file);

  WishListFromFile := wlp;
END;

PROCEDURE WriteList(wlp: WishListPtr);
VAR node: WishNodePtr;
BEGIN
  IF wlp <> nil THEN BEGIN
    node := wlp^.next;
    WHILE node <> wlp DO BEGIN
      WriteLn('(', node^.n,') ', node^.wish);
      node := node^.next;
    END;
  END;
END;

FUNCTION DisposeWishList(wlp: WishListPtr): BOOLEAN;
VAR node: WishNodePtr;
BEGIN
  DisposeWishList := false;
  IF wlp <> nil THEN BEGIN (* list does exist *)

    IF wlp^.next = wlp THEN (* list is empty *)
      Dispose(wlp)
    ELSE BEGIN (* list is not empty *)
      node := wlp^.next;
      WHILE node <> wlp DO BEGIN
        node := node^.next;
        Dispose(node^.pred);
      END;
      Dispose(wlp);
    END; (* ! list is not empty *)
    DisposeWishList := true;

  END; (* ! list does exist *)
END;

BEGIN END.
