PROGRAM WishListAnalyser;

USES Crt, WishList;

VAR
  simple_append,
  alpha_sort,
  common_first: WishListPtr;

  file_path: String;

BEGIN
  file_path := 'Wishes.txt';

  (* Can't tell me I have not learned that yet ;)            *)
  (* Just passing objects like a noob, we did that in class  *)
  simple_append := WishListFromFile(file_path, InsertBack);
  alpha_sort := WishListFromFile(file_path, InsertAlphabetically);
  common_first := WishListFromFile(file_path, InsertFront);

  WriteLn('All items in order of occurence: ');
  WriteList(simple_append);
  WriteLn;
  WriteLn('All items sorted alphabetically: ');
  WriteList(alpha_sort);
  WriteLn;
  WriteLn('All items with the "most common" ones first: ');
  WriteList(common_first);

  DisposeWishList(simple_append);
  DisposeWishList(alpha_sort);
  DisposeWishList(common_first);
END.
