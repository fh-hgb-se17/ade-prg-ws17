PROGRAM CharListTests;

USES CharList;

PROCEDURE WriteExtendedString(VAR str: ExtendedString; size: INTEGER);
VAR i: INTEGER;
BEGIN
  FOR i := 0 TO Size-1 DO
    Write(str[i]);
  WriteLn;
END;

VAR
  my_list,
  someone_elses_list,
  our_list: CharListPtr;

  oversized_list: CharListPtr;
  i: INTEGER;

  ext: ExtendedString;
  ext_length: INTEGER;

BEGIN
  (* initialization *)
  my_list := CharListOf('By the nine!');
  someone_elses_list := CharListOf('It''s 10 o''clock already!');
  (* concatenation *)
  our_list := CLConcat(my_list, someone_elses_list);
  WriteList(my_list);
  WriteList(someone_elses_list);
  WriteList(our_list);

  (* size *)
  WriteLn('Size of my_list: ', CLLength(my_list));
  WriteLn('Size of someone_elses_list: ', CLLength(someone_elses_list));
  WriteLn('Size of our_list: ', CLLength(our_list));

  (* to String *)
  WriteLn('String version of our_list: ', StringOf(our_list));

  (* list > 255 *)
  (* 'NewCharList' over here like "I AM DOING SOMETHING *0*" *)
  oversized_list := NewCharList;
  FOR i := 1 TO 300 DO BEGIN
    Append(oversized_list, '!');
  END;
  WriteList(oversized_list);
  WriteLn('Size of oversized_list: ', CLLength(oversized_list));
  FullStringOf(oversized_list, ext, ext_length);
  Write('String version of oversized_list: ');
  WriteExtendedString(ext, ext_length);
  WriteLn('Size of String version: ', ext_length);

  (* relation *)
  WriteLn('my_list > someone_elses_list: ', Relation(my_list, someone_elses_list), ' (0=equal,1=yes,-1=no)');

  (* deletion *)
  DisposeCharList(my_list);
  DisposeCharList(someone_elses_list);
  DisposeCharList(our_list);
  DisposeCharList(oversized_list);
  FreeMem(ext, ext_length*SizeOf(CHAR));
  WriteLn('my_list still exists: ', (my_list <> nil));
END.
