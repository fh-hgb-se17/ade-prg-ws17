UNIT CharList;


INTERFACE

TYPE
  CharNodePtr = ^CharNode;
  CharNode = RECORD
    ch: CHAR;
    next: CharNodePtr;
  END;
  CharListPtr = CharNodePtr;
  ExtendedString = ^CHAR;

(*returns empty CharList*)
FUNCTION NewCharList: CharListPtr;

(*disposes all nodes and sets cl to empty CharList*)
PROCEDURE DisposeCharList(VAR cl: CharListPtr);

(*returns the number of characters in cl*)
FUNCTION CLLength(cl: CharListPtr): INTEGER;

(*returns concatenation of cl1 and cl2 by copying the nodes of both lists*)
FUNCTION CLConcat(cl1, cl2: CharListPtr): CharListPtr;

(*returns CharList representation of STRING*)
FUNCTION CharListOf(s: STRING): CharListPtr;

(*returns STRING representation of CharList, may result in truncatation*)
FUNCTION StringOf(cl: CharListPtr): STRING;

(* returns the full string as C-Style-String (^char)  *)
(* along with the size of it.                         *)
(* Note: this array must be deallocated manually!     *)
PROCEDURE FullStringOf(cl: CharListPtr;
                      VAR s: ExtendedString;
                      VAR size: INTEGER);

(*returns 0 if equal, -1 if first one is "smaller", +1 if it is "bigger"*)
FUNCTION Relation(cl1, cl2: CharListPtr) : INTEGER;

(* === THIS PART OF THE INTERFACE IS FOR DEBUGGING PURPOSES ONLY === *)
(* === IT IS NOT PART OF THE ASSIGNMENT                          === *)
(*prints list to stdout*)
PROCEDURE WriteList(cl: CharListPtr);

(*appends a char to the list and returns a pointer to it*)
FUNCTION Append(VAR cl: CharListPtr; ch: CHAR) : CharNodePtr;


IMPLEMENTATION

(* private utility function *)
FUNCTION NewNode(ch: CHAR): CharNodePtr;
VAR node: CharNodePtr;
BEGIN
  New(node);
  node^.ch := ch;
  node^.next := nil;
  NewNode := node;
END;

(* private utility function *)
FUNCTION CopyCharList(cl: CharListPtr): CharListPtr;
VAR
  list: CharListPtr;
  node_to_copy, copied_node: CharNodePtr;
BEGIN
  list := nil;
  IF cl <> nil THEN BEGIN
    node_to_copy := cl;
    (* initialize list with first node of list to copy *)
    list := NewNode(node_to_copy^.ch);
    copied_node := list;
    (* as long as that node has successors *)
    WHILE node_to_copy^.next <> nil DO BEGIN
      (* create an equal successor for the copied list *)
      copied_node^.next := NewNode(node_to_copy^.next^.ch);
      copied_node := copied_node^.next;
      node_to_copy := node_to_copy^.next;
    END;
  END;
  CopyCharList := list;
END;

(* private utility function *)
FUNCTION LastNodeOf(cl: CharListPtr): CharNodePtr;
VAR node: CharNodePtr;
BEGIN
  node := cl;
  IF (cl <> nil) AND (node^.next <> nil) THEN
    node := LastNodeOf(node^.next);
  LastNodeOf := node;
END;

(* This has to be the most useless function ever.   *)
(* A getter and setter simply assigning a value to a*)
(* public data member would be of more use..        *)
(* ... like what the actual flying christ even..    *)
FUNCTION NewCharList: CharListPtr;
VAR list: CharListPtr;
BEGIN
  list := nil;
  NewCharList := list;
END;

PROCEDURE DisposeCharList(VAR cl: CharListPtr);
VAR node, succ: CharNodePtr;
BEGIN
  IF cl <> nil THEN BEGIN
    node := cl;
    succ := node^.next;
    WHILE (node <> nil) DO BEGIN
      succ := node^.next;
      Dispose(node);
      node := succ;
    END;
    cl := nil;
  END;
END;

FUNCTION CLLength(cl: CharListPtr): INTEGER;
VAR length: INTEGER;
BEGIN
  length := 0;
  WHILE (cl <> nil) DO BEGIN
    length+=1;
    cl := cl^.next;
  END;
  CLLength := length;
END;

FUNCTION CLConcat(cl1, cl2: CharListPtr): CharListPtr;
VAR new_list: CharListPtr;
BEGIN
  new_list := nil;
  IF (cl1 <> nil) AND (cl2 <> nil) AND (cl1 <> cl2) THEN BEGIN
    new_list := CopyCharList(cl1);
    LastNodeOf(new_list)^.next := CopyCharList(cl2);
  END;
  CLConcat := new_list;
END;

FUNCTION CharListOf(s: STRING): CharListPtr;
VAR
  cl: CharListPtr;
  pred, node: CharNodePtr;
  i: INTEGER;
BEGIN
  cl := nil;
  IF s <> '' THEN BEGIN
    cl := NewNode(s[1]);
    pred := cl;
    FOR i := 2 TO Length(s) DO BEGIN
      node := NewNode(s[i]);
      pred^.next := node;
      pred := node;
    END;
  END;
  CharListOf := cl;
END;

FUNCTION StringOf(cl: CharListPtr): String;
VAR
  s: String;
  i, upper: INTEGER;
  node: CharNodePtr;
BEGIN
  s := '';
  node := cl;
  upper := CLLength(cl);

  IF upper > 255 THEN upper := 255;

  FOR i := 1 TO upper DO BEGIN
    s += node^.ch;
    node := node^.next;
  END;
  StringOf := s;
END;

PROCEDURE FullStringOf(cl: CharListPtr;
                      VAR s: ExtendedString;
                      VAR size: INTEGER);
VAR
  node: CharNodePtr;
  i: INTEGER;
BEGIN
  IF cl <> nil THEN BEGIN
    GetMem(s, CLLength(cl)*SizeOf(CHAR));
    node := cl;
    size := CLLength(cl);
    FOR i := 0 TO size-1 DO BEGIN
      s[i] := node^.ch;
      node := node^.next;
    END;
  END;
END;

FUNCTION Relation(cl1, cl2: CharListPtr) : INTEGER;
VAR cl1_node, cl2_node: CharNodePtr;
BEGIN
  Relation := 0;
  IF (cl1 <> nil) AND
    (cl2 <> nil) AND
    (cl1 <> cl2) THEN BEGIN

      cl1_node := cl1;
      cl2_node := cl2;

      (* "as long as the characters match and there is a next character" *)
      WHILE (cl1_node^.ch = cl2_node^.ch) AND
        (cl1_node^.next <> nil) AND
        (cl2_node^.next <> nil) DO BEGIN
          cl1_node := cl1_node^.next;
          cl2_node := cl2_node^.next;
      END;

      (* if the lists match until the last character of one of the lists *)
      IF (cl1_node^.ch = cl2_node^.ch) THEN BEGIN
        (* Relation('Hello','Hello m8') *)
        IF (cl1_node^.next = nil) AND (cl2_node^.next <> nil) THEN Relation := -1
        (* Relation('Hello m8','Hello') *)
        ELSE IF (cl2_node^.next = nil) AND (cl1_node^.next <> nil) THEN Relation := 1;
      END
      ELSE BEGIN (* if they did not match at one point *)
        (* Relation('Hallo', 'Hello') *)
        IF cl1_node^.ch < cl2_node^.ch THEN Relation := -1
        (* Relation('Hello', 'Hallo') *)
        ELSE Relation := 1;
      END;

  END;
END;

PROCEDURE WriteList(cl: CharListPtr);
VAR node: CharNodePtr;
BEGIN
  node := cl;
  Write('[ ');
  WHILE node <> nil DO BEGIN
    Write(node^.ch, ' ');
    node := node^.next;
  END;
  WriteLn(']');
END;

FUNCTION Append(VAR cl: CharListPtr; ch: CHAR) : CharNodePtr;
VAR node: CharNodePtr;
BEGIN
  node := nil;
  IF ch <> '' THEN BEGIN
    node := NewNode(ch);
    IF (cl <> nil) THEN
      LastNodeOf(cl)^.next := node
    ELSE
      cl := node;
  END;
  Append := node;
END;

(* No initialization necesasry for this module, but the *)
(* compiler complains it this part is left out *)
BEGIN END.
