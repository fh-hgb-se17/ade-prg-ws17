PROGRAM Edgar;

VAR
  zufallsZahl, tipp : INTEGER;
  versuche : INTEGER;

BEGIN
  Randomize;

  zufallsZahl := (Random(2500) + 1) * 10;

  (* For debugging purposes only *)
  WriteLn('Die Zufallszahl ist ', zufallsZahl, ', aber nicht weitersagen!');

  Write('Was denkst du ist die heutige Tagessumme? > ');

  ReadLn(tipp);
  versuche := 1;

  WHILE tipp <> zufallsZahl DO BEGIN
    (* The apostrophe denoting a string can be escaped using a second apostrophe *)
    IF tipp < zufallsZahl THEN Write('Versuch''s mit einer hoeheren Zahl! > ')
    ELSE Write('Versuch''s mit einer niedrigeren Zahl! > ');

    ReadLn(tipp);
    versuche+=1;
  END;

  WriteLn('*Edgar wiehert* Anzahl an benoetigten Versuchen: ', versuche);

END.
