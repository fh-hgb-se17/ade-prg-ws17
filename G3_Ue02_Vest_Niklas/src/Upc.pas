PROGRAM Upc;

CONST
  MAX_DIGITS = 12;

VAR
  (* The field holding the digits *)
  digits : ARRAY[0..MAX_DIGITS] OF INTEGER;

  checkDigit, (* storage for the final check digit *)
  currentDigit, (* index variable for field 'digits' *)
  singleOrFull, (* indicator for input mode *)
  sumO, sumE, sumTotal : INTEGER; (* calculation results *)

  (* storage for parsing the full UPC
  64bit int because default INTEGER is only 16bit
  (too few for a 12-digit number) *)
  upcParserBuffer : Int64;

BEGIN

  (* Decide which input mode to use *)
  singleOrFull := -1;
  Write('Would you like to enter you UPC digit by',
    ' digit (0) or as full code (1)? (0/1) > ');
  Read(singleOrFull);

  (*
  Read UPC depending on input mode
  *)
  IF singleOrFull = 0 THEN BEGIN (* Read digits separately (3.a) *)
    (* -2 because we do not want the check digit when reading digits separately*)
    FOR currentDigit := 0 TO MAX_DIGITS-2 DO BEGIN
      Write('Digit ', currentDigit+1, '> ');
      Read(digits[currentDigit]);
    END;
  END
  ELSE IF singleOrFull = 1 THEN BEGIN (* Read full UPC code (3.b) *)

    Write('Enter UPC (You can leave out leading zeroes!): ');
    Read(upcParserBuffer);
    IF upcParserBuffer < 0 THEN BEGIN
      WriteLn('No negative numbers! Exiting...');
      EXIT;
    END;

    (* Cut rightmost digit and add it to the field *)
    currentDigit := MAX_DIGITS-1;
    WHILE upcParserBuffer / 10 <> 0 DO BEGIN
      digits[currentDigit] := upcParserBuffer MOD 10;
      currentDigit-=1;
      upcParserBuffer := upcParserBuffer DIV 10;
    END;

    (* Add leading zeroes *)
    WHILE currentDigit >= 0 DO BEGIN
      digits[currentDigit] := 0;
      currentDigit-=1;
    END;

  END
  ELSE BEGIN (* Terminate *)
    WriteLn('You had one job..');
    EXIT;
  END;

  (*
  Check digit calculation
  *)
  sumO := 0;
  sumE := 0;

  FOR currentDigit := 0 TO MAX_DIGITS-2 DO BEGIN
    IF currentDigit MOD 2 = 0 THEN BEGIN
      (* Add odd digits and multiply the sum by 3 (3.1) *)
      sumO += digits[currentDigit]*3;
    END
    ELSE BEGIN
      (* Add even digits (3.2)*)
      sumE += digits[currentDigit];
    END;
  END;

  sumTotal := sumO + sumE;

  (* 3.3 *)
  checkDigit := 0;
  IF sumTotal MOD 10 <> 0 THEN checkDigit := 10 - (sumTotal MOD 10);
  digits[MAX_DIGITS-1] := checkDigit; (* Add it for completeness
                                      -> never referenced*)

  (* Print result *)
  Write('Check digit for UPC ');
  FOR currentDigit := 0 TO MAX_DIGITS-2 DO Write(digits[currentDigit]);
  Write('X is ', checkDigit);

END.
