PROGRAM Primfaktorzerlegung;

(* I HAVE THE POWER lol *)
Uses Math;

VAR
  input, nextPrime, smaller : INTEGER;

  (* simple formatting flag *)
  first : BOOLEAN;

BEGIN

  Write('Enter a number x where 2 <= x <= 32 767: ');
  Read(input);
  IF input < 2 THEN BEGIN
    WriteLn('Boi..');
    EXIT;
  END;

  first := TRUE;

  (* as long as the input has not been divided down to 1 *)
  WHILE input <> 1 DO BEGIN

    (* find a prime number that divides the
    number currently stored in input.
    -> This is the hammer method, inefficient for higher numbers *)
    nextPrime := 2;

    (* loops until next prime that divides 'input' has been found *)
    WHILE input MOD nextPrime <> 0 DO BEGIN

      smaller := 2;
      nextPrime += 1;
      (* loops until next prime has been found *)
      WHILE smaller < nextPrime DO BEGIN

        IF nextPrime MOD smaller = 0 THEN BEGIN
          nextPrime+=1; (* This is not a prime, increment and *)
          smaller := 2; (* restart loop *)
        END
        ELSE smaller+= 1;

      END;
    END;

    input := input DIV nextPrime;

    (* Print next prime factor *)
    IF first THEN BEGIN
      first := FALSE;
      Write(nextPrime);
    END ELSE Write(', ', nextPrime);

  END;

END.
