PROGRAM NumFuncs;

Uses Math; (* didn't bother copying the power func from earlier exercises *)

CONST
  ALL_CHARS : ARRAY[0..35] OF CHAR = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
                                      'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                                      'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

(* returns the decimal value of a given string which has the radix <base> *)
FUNCTION ValueOf(digits: STRING; base: INTEGER): INTEGER;
VAR val, curr_dig, i, buffer : INTEGER;
BEGIN

  val := 0;
  curr_dig := 1;
  WHILE (curr_dig <= Length(digits)) AND (val <> -1) DO BEGIN

    (* find the decimal value for the current digit in the string *)
    buffer := -1;
    FOR i := 0 TO High(ALL_CHARS) DO BEGIN
      (* if the character is a valid representation of a decimal value *)
      IF (ALL_CHARS[i] = digits[curr_dig]) AND (i < base) THEN
        (* use the index as decimal value and multiply with "digit-weight" *)
        buffer := i * Round(Power(base, Length(digits)-curr_dig));
    END;

    (* if that string contained any characters besides the ones inside ALL_CHARS *)
    IF buffer = -1 THEN BEGIN
      val := -1; (* Exit loop *)
      WriteLn('(', digits, ')_', base, ' has an invalid digit: ', digits[curr_dig]);
    END
    (* if the number is not invalid yet *)
    ELSE val += buffer;

      curr_dig += 1;
  END;

  ValueOf := val;
END;

(* returns a string representing a number with the radix <base> *)
FUNCTION DigitsOf(value: INTEGER; base: INTEGER): STRING;
VAR i : INTEGER;
  full : String;
  dig : CHAR;
BEGIN
  full := '';
  WHILE value <> 0 DO BEGIN
    (* append digit according to decimal value *)
    full += ALL_CHARS[value MOD base];
    (* continuously divide value by target base *)
    value := value DIV base;
  END;

  (* reverse that string *)
  FOR i := 1 TO (Length(full) DIV 2) DO BEGIN
    dig := full[i];
    full[i] := full[Length(full)-i+1];
    full[Length(full)-i+1] := dig;
  END;

  DigitsOf := full;
END;

FUNCTION Sum(v1 : STRING; b1 : INTEGER; v2 : STRING; b2 : INTEGER): INTEGER;
BEGIN
  Sum := ValueOf(v1, b1) + ValueOf(v2, b2);
END;
FUNCTION Diff(v1 : STRING; b1 : INTEGER; v2 : STRING; b2 : INTEGER): INTEGER;
BEGIN
  Diff := ValueOf(v1, b1) - ValueOf(v2, b2);
END;
FUNCTION Prod(v1 : STRING; b1 : INTEGER; v2 : STRING; b2 : INTEGER): INTEGER;
BEGIN
  Prod := ValueOf(v1, b1) * ValueOf(v2, b2);
END;
FUNCTION Quot(v1 : STRING; b1 : INTEGER; v2 : STRING; b2 : INTEGER): INTEGER;
BEGIN
  Quot := ValueOf(v1, b1) DIV ValueOf(v2, b2);
END;


BEGIN
  WriteLn('(1001)_2 = (', ValueOf('1001', 2), ')_10');
  WriteLn('(1A)_16 = (', ValueOf('1A', 16), ')_10');
  WriteLn('(77)_8 = (', ValueOf('77', 8), ')_10');
  WriteLn('(38)_10 = (', ValueOf('38', 10), ')_10');

  WriteLn('10 to base 2 is ', DigitsOf(10, 2));
  WriteLn('10 to base 16 is ', DigitsOf(10, 16));
  WriteLn('10 to base 7 is ', DigitsOf(10, 7));
  WriteLn('10 to base 3 is ', DigitsOf(10, 3));

  WriteLn('(1001)_2 + (10)_3 = (9)_10 + (3)_10 = ', Sum('1001', 2, '10', 3));
  WriteLn('(1001)_2 - (10)_3 = (9)_10 - (3)_10 = ', Diff('1001', 2, '10', 3));
  WriteLn('(1001)_2 * (10)_3 = (9)_10 * (3)_10 = ', Prod('1001', 2, '10', 3));
  WriteLn('(1001)_2 / (10)_3 = (9)_10 / (3)_10 = ', Quot('1001', 2, '10', 3));
END.
