PROGRAM Matrices;

TYPE
  Mat3 = ARRAY[0..2,0..2] OF REAL;

(* Using VAR to reduce stack size, would use CONSTREF since the matrix
isn't mutated within the procedure hence the compiler could make some
optimizations but that would mean less points for the assignment :'( *)
PROCEDURE PrintMat3(VAR m : Mat3);
VAR
  row, col : INTEGER;
BEGIN
  FOR row := 0 TO 2 DO BEGIN
    Write('|');
    FOR col := 0 TO 2 DO BEGIN
      (* 6 digits, 2 after decimal point *)
      Write(' ', m[row][col]:6:2, ' ');
    END;
    WriteLn('|');
  END;
END;

PROCEDURE ReadMat3(VAR m : Mat3);
  VAR
    row, col : INTEGER;
BEGIN
  WriteLn('== Creating new matrix ==');
  FOR row := 0 TO 2 DO
    FOR col := 0 TO 2 DO BEGIN
      Write('Row ', row+1, ', Column ', col+1, ': ');
      (* read values row-wise *)
      ReadLn(m[row][col]);
    END;
END;

(* mulitplies two matrices and stores result in m1 *)
PROCEDURE Mat3Prod(m1, m2 : Mat3; VAR m3 : Mat3);
VAR
  row, col, calc_ind : INTEGER;
  scalar : REAL;
  mat_buffer : Mat3;
BEGIN
  FOR row := 0 TO 2 DO BEGIN
    FOR col := 0 TO 2 DO BEGIN
      scalar := 0;
      (* calculate scalar product of the left hand side's maxtrix' row and
      the right hand side's matrix' column *)
      FOR calc_ind := 0 TO 2 DO BEGIN
        scalar += m1[row][calc_ind] * m2[calc_ind][col];
      END;
      mat_buffer[row][col] := scalar;
    END;
  END;

  m3 := mat_buffer;
END;

VAR
  m1, m2 : Mat3;

BEGIN
  ReadMat3(m1);
  ReadMat3(m2);

  WriteLn('Multyplying matrix A:');
  PrintMat3(m1);
  WriteLn('With matrix B:');
  PrintMat3(m2);

  Mat3Prod(m1, m2, m1);

  WriteLn('Result A * B:');
  PrintMat3(m1);
END.
