PROGRAM Bardiagram;

CONST
  ROWS = 10;
  MAX_COLS = 39;

TYPE
  IntArr = ARRAY[0..MAX_COLS] OF INTEGER;

PROCEDURE BarChart(ch : CHAR; VAR nums : IntArr; size : INTEGER);
VAR
  i, arr_ind : INTEGER;
  one_larger_or_eaqual : BOOLEAN;
BEGIN

  (* for each row *)
  FOR i := ROWS DOWNTO 1 DO BEGIN

    arr_ind := 0;
    one_larger_or_eaqual := FALSE;
    (* test all elements in array *)
    WHILE (arr_ind < size) AND NOT one_larger_or_eaqual DO BEGIN
      (* if at least one element is as high as the current row (or higher) *)
      IF nums[arr_ind] >= i THEN
        (* stop loopin' *)
        one_larger_or_eaqual := TRUE;
      arr_ind += 1;
    END;

    (* print the line *)
    IF one_larger_or_eaqual THEN BEGIN
      Write(i:2, '| ');

      (* the actual bars*)
      FOR arr_ind := 0 TO size-1 DO BEGIN
        IF nums[arr_ind] >= i THEN Write(ch, '  ') ELSE Write('   ');
      END;
      WriteLn;
    END;
  END;

  (* pure formatting from here *)
  Write('  + ');
  FOR arr_ind := 0 TO size-1 DO BEGIN
    Write('---')
  END;
  WriteLn;
  Write('   ');

  FOR arr_ind := 1 TO size DO BEGIN
    Write(arr_ind:2, ' ');
  END;
END;

VAR
  printable : CHAR;
  numbers : IntArr;
  current : INTEGER;

BEGIN
  Write('Character: ');
  ReadLn(printable);

  WriteLn('Enter "0" to display the diagram with entered values.');

  current := -1;
  REPEAT BEGIN
    current += 1;
    Write('Enter number ', current+1, ' (out of ', MAX_COLS+1, ') or 0 to display the diagram: ');
    ReadLn(numbers[current]);
  END
  UNTIL (current = MAX_COLS) OR (numbers[current] = 0);

  BarChart(printable, numbers, current);
END.
