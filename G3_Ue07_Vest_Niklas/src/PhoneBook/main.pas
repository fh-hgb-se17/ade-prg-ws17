PROGRAM PhoneBookPrgm;

USES PhoneBookLib;

VAR ok: BOOLEAN;
    entr: Entry;

BEGIN

  (* Add *)
  entr.firstName := 'Niklas';
  entr.lastName := 'Vest';
  entr.phoneNumber := 1802311169;
  StoreEntry(entr, ok);

  (* Add *)
  entr.firstName := 'Felix';
  entr.lastName := 'Troebinger';
  entr.phoneNumber := 2102618127;
  StoreEntry(entr, ok);

  (* Add *)
  entr.firstName := 'Kelleh';
  entr.lastName := 'Trinh';
  entr.phoneNumber := 32487438;
  StoreEntry(entr, ok);

  (* Overwrite *)
  entr.firstName := 'Felix';
  entr.lastName := 'Troebinger';
  entr.phoneNumber := 1168431900;
  StoreEntry(entr, ok);

  (* Add (should not be actually be added
  since the list is full at this point) *)
  entr.firstName := 'Gundula';
  entr.lastName := 'Megaboi';
  entr.phoneNumber := 632573218;
  StoreEntry(entr, ok);

  (* Should remove the entry *)
  RemoveEntry('Niklas', 'Vest', ok);
  WriteLn('Removed Niklas Vest: ', ok, ', entries left: ', NumberOfEntries);

  (* Sould do nothing *)
  RemoveEntry('Gundula', 'Megaboi', ok);
  WriteLn('Removed Gundula Megaboi: ', ok, ', entries left: ', NumberOfEntries);

  WriteLn('Felix'' phone number is ', NumberFor('Felix', 'Troebinger'));

  entr.firstName := 'Hengst';
  entr.lastName := 'Super';
  entr.phoneNumber := 69696969;
  StoreEntry(entr, ok);

  WriteLn;
  WritePhoneBook;
END.
