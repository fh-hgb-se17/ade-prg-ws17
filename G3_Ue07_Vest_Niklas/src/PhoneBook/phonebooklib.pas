UNIT PhoneBookLib;

INTERFACE

  CONST
    (* Professional global namespace pollution.exe *)
    MAX_ENTRIES = 3;
    ENTRY_NOT_FOUND = -1;

  TYPE
    Entry = RECORD
      firstName: String[20];
      lastName: String[30];
      phoneNumber: LONGINT;
    END;
    PhoneBook = ARRAY[1..MAX_ENTRIES] OF Entry;
    PhoneNumber = LONGINT;
    FirstNameStr = String[20];
    LastNameStr = String[30];


  (*  Adds passed entry to the phone book and overwrites current *)
  (*  phone number if the number of this person is already stored. *)
  (*    Parameter 'ok' is set to false if the phone book is full. *)
  PROCEDURE StoreEntry(entry: Entry; VAR ok: BOOLEAN);



  (*  Returns the telephone number associated with the name passed *)
  (*  as (separate) parameters *)
  (*    If the name is not found within the phone book, returns *)
  (*    ENTRY_NOT_FOUND. *)
  FUNCTION NumberFor(first: FirstNameStr; last: LastNameStr) : PhoneNumber;


  (*  Removes an entry from the phone book. *)
  (*    Parameter 'ok' is set to false if there is no entry for *)
  (*    this person in the phone book. *)
  PROCEDURE RemoveEntry(first: FirstNameStr; last: LastNameStr; VAR ok: BOOLEAN);


  (* Prints all entries of the phone book. *)
  PROCEDURE WritePhoneBook;


  (*  Returns the number of entries currently stored in *)
  (*  the phone book. *)
  FUNCTION NumberOfEntries : INTEGER;


IMPLEMENTATION

  VAR phoneBookInstance : PhoneBook;
      entryCount: INTEGER;


  (* Private utility function *)
  FUNCTION IndexFor(first: FirstNameStr; last: LastNameStr) : INTEGER;
  VAR i: INTEGER;
  BEGIN
    i := Low(phoneBookInstance);
    WHILE (i <= entryCount) AND
      ((first <> phoneBookInstance[i].firstName) OR
      (last <> phoneBookInstance[i].lastName)) DO
        i+=1;

    IF i > entryCount THEN i := ENTRY_NOT_FOUND;
    IndexFor := i;
  END;


  (* Private utility function *)
  PROCEDURE Invalidate(index: INTEGER);
  BEGIN
    WITH phoneBookInstance[index] DO BEGIN
      (* Just invalidate if last entry should be deleted *)
      firstName := '';
      lastName := '';
      phoneNumber := 0;
    END;
  END;


  PROCEDURE StoreEntry(entry: Entry; VAR ok: BOOLEAN);
  BEGIN
    ok := true;
    (* Might be a good idea to check entr.firstName and
    entr.lastName for empty string, but that's not part
    of the assignment specification :P *)

    // in case the intention was to append the entry
    IF NumberFor(entry.firstName, entry.lastName) = ENTRY_NOT_FOUND THEN BEGIN
      IF entryCount >= High(phoneBookInstance) THEN ok := false
      ELSE BEGIN
        entryCount += 1;
        phoneBookInstance[entryCount] := entry;
      END;
    END ELSE // in case the intention was to overwrite the entry
      phoneBookInstance[IndexFor(entry.firstName,entry.lastName)].phoneNumber := entry.phoneNumber;
  END;


  FUNCTION NumberFor(first: FirstNameStr; last: LastNameStr) : LONGINT;
  VAR i: INTEGER;
      phoneNumber: LONGINT;
  BEGIN
    i := Low(phoneBookInstance);
    phoneNumber := ENTRY_NOT_FOUND;
    WHILE (i <= entryCount) AND (phoneNumber = ENTRY_NOT_FOUND) DO BEGIN
      (* not using WITH statement because it would hurt readability *)
      IF (first = phoneBookInstance[i].firstName)
        AND (last = phoneBookInstance[i].lastName) THEN
        phoneNumber := phoneBookInstance[i].phoneNumber;
        i += 1;
    END;
    NumberFor := phoneNumber;
  END;


  PROCEDURE RemoveEntry(first: FirstNameStr; last: LastNameStr; VAR ok: BOOLEAN);
  VAR i, entryIndex: INTEGER;
  BEGIN
    ok := false;
    IF NumberFor(first, last) <> ENTRY_NOT_FOUND THEN BEGIN
      entryIndex := IndexFor(first, last);
      IF entryIndex <> entryCount THEN BEGIN
        FOR i := entryIndex TO entryCount-1 DO
          phoneBookInstance[i] := phoneBookInstance[i+1]
      END;
      (* The last entry that was valid until this point
      should always be invalidated *)
      Invalidate(entryCount);
      entryCount -= 1;
      ok := true;
    END;
  END;


  PROCEDURE WritePhoneBook;
  VAR i: INTEGER;
  BEGIN
    WriteLn('=== Phonebookinat0r 3000 ===');
    FOR i := Low(phoneBookInstance) TO entryCount DO BEGIN
      WITH phoneBookInstance[i] DO BEGIN
        WriteLn('Name: ', lastName, ', ', firstName);
        WriteLn('Phone Nr.: ', phoneNumber);
      END;
      WriteLn('============================');
    END;
  END;


  FUNCTION NumberOfEntries : INTEGER;
  BEGIN
    NumberOfEntries := entryCount;
  END;

BEGIN
    entryCount := 0;
END.
