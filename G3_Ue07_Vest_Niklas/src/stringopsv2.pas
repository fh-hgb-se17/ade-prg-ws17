PROGRAM StringOperationsV2;

FUNCTION WithoutLastChar(s: String) : String;
BEGIN
  IF s = '' THEN WithoutLastChar := ''
  ELSE WithoutLastChar := Copy(s, 1, Length(s)-1);
END;

PROCEDURE ReplaceAll(VAR s: String; old, new: String);
VAR sp : INTEGER;
    before_substr, after_substr : String;
BEGIN
  sp := Pos(old, s);
  IF sp <> 0 THEN BEGIN
    before_substr := Copy(s, 1, sp-1);
    after_substr := Copy(s, sp+Length(old), Length(s)-sp+Length(old)+1);
    s := Concat(before_substr, new, after_substr);
    ReplaceAll(s, old, new);
  END;
END;

PROCEDURE StripBlank(VAR s: String);
BEGIN
  (* 1080 noscope *)
  ReplaceAll(s, ' ','');
END;

VAR s: String;
BEGIN
  s := 'Thisreplace isreplace areplace normalreplace sentence.';
  ReplaceAll(s, 'replace', '.');
  WriteLn(s);
  ReplaceAll(s, ',', ' COMMA ');
  WriteLn(s);
  StripBlank(s);
  WriteLn(s);
  WriteLn(WithoutLastChar('This is Hello'));
  WriteLn(WithoutLastChar(''));
END.
