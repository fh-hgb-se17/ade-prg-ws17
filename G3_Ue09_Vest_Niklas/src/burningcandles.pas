PROGRAM BurningCandles;


FUNCTION XFire(base: INTEGER): INTEGER;
VAR ways: INTEGER;
BEGIN
  ways := -1; (* -1 indicates invalid argument *)
  IF (base MOD 2 <> 0) AND (base > 1) THEN BEGIN

    IF base > 3 THEN
      ways := 1 + 2 * XFire(base-2)
    ELSE
      ways := 3;

  END ELSE IF base = 1 THEN
    Write('I will not burn a sappling. ')
  ELSE
    Write('Can''t burn that tree. ');
  XFire := ways;
END;

FUNCTION XFireIterative(base: INTEGER): INTEGER;
VAR i, ways: INTEGER;
BEGIN
  ways := -1; (* -1 indicates invalid argument *)
  IF (base MOD 2 <> 0) AND (base > 1) THEN BEGIN
    ways := 3;
    (* DIV truncates *)
    FOR i := 3 TO base DIV 2 + 1 DO ways := ways * 2 + 1;
  END ELSE IF base = 1 THEN
    Write('I will not burn a sappling. ')
  ELSE
    Write('Can''t burn that tree. ');
  XFireIterative := ways;
END;

BEGIN
  WriteLn('[Recursive] Ways to burn "EGG": ',
    XFire(Length('EGG')));
  WriteLn('[Recursive] Ways to burn "HELLO": ',
    XFire(Length('HELLO')));
  WriteLn('[Recursive] Ways to burn "GALILEO" at the stake: ',
    XFire(Length('GALILEO')));
  WriteLn('[Iterative] Ways to burn "HALLELUJA": ',
    XFire(Length('HALLELUJA')));
  WriteLn('[Recursive] Ways to burn "I": ',
    XFire(Length('I')));
  WriteLn('[Recursive] Ways to burn an imaginary tree: ',
    XFire(Length('')));
  WriteLn;
  WriteLn('[Iterative] Ways to burn "EGG": ',
    XFireIterative(Length('EGG')));
  WriteLn('[Iterative] Ways to burn "HELLO": ',
    XFireIterative(Length('HELLO')));
  WriteLn('[Iterative] Ways to burn "GALILEO" at the stake: ',
    XFireIterative(Length('GALILEO')));
  WriteLn('[Iterative] Ways to burn "HALLELUJA": ',
    XFireIterative(Length('HALLELUJA')));
  WriteLn('[Iterative] Ways to burn "I": ',
    XFireIterative(Length('I')));
  WriteLn('[Iterative] Ways to burn an imaginary tree: ',
    XFireIterative(Length('')));
END.
