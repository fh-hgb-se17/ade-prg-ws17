PROGRAM CandlePrgm;


FUNCTION Candles(height: INTEGER): REAL;
VAR _candles: REAL;
BEGIN
  _candles := -1; (* -1 indicates invalid argument *)
  IF height = 1 THEN
    _candles := 1
  ELSE IF height > 1 THEN
    _candles := Candles(height-1) * 3 + 1
  ELSE
    WriteLn('Invalid parameter: height must be greater than 0!');
  Candles := _candles;
END;

FUNCTION CandlesIterative(height: INTEGER): REAL;
VAR
  i: INTEGER;
  _candles: REAL;
BEGIN
  _candles := -1; (* -1 indicates invalid argument *)
  IF height >= 1 THEN BEGIN
    _candles := 1;
    FOR i := 2 TO height DO BEGIN
      _candles*=3;
      _candles+=1;
    END;
  END ELSE
    WriteLn('Invalid parameter: height must be greater than 0!');

  CandlesIterative := _candles;
END;

BEGIN
  WriteLn('[Recursive] A tree of height 1 has room for ',
    Candles(1):4:0, ' candles.');
  WriteLn('[Recursive] A tree of height 2 has room for ',
    Candles(2):4:0, ' candles.');
  WriteLn('[Recursive] A tree of height 3 has room for ',
    Candles(3):4:0, ' candles.');
  WriteLn('[Recursive] A tree of height 6 has room for ',
    Candles(6):4:0, ' candles.');

  WriteLn('[Iterative] A tree of height 1 has room for ',
    Candles(1):4:0, ' candles.');
  WriteLn('[Iterative] A tree of height 2 has room for ',
    Candles(2):4:0, ' candles.');
  WriteLn('[Iterative] A tree of height 3 has room for ',
    Candles(3):4:0, ' candles.');
  WriteLn('[Iterative] A tree of height 6 has room for ',
    Candles(6):4:0,
    ' candles. That''s a lot of candles m8!');
END.
