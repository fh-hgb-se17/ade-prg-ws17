UNIT WishList;


INTERFACE

TYPE

  ChildNodePtr = ^ChildNode;
  ChildNode = RECORD
    name: String;
    next: ChildNodePtr;
  END;
  ChildListPtr = ChildNodePtr;

  WishNodePtr = ^WishNode;
  WishNode = RECORD
    pred, next: WishNodePtr;
    wish: STRING;
    n: INTEGER;
    children: ChildListPtr;
  END;

  (* Acts as anchor *)
  WishListPtr = WishNodePtr;

  WLInserter = PROCEDURE(wlp: WishListPtr; child, wish: String);


(* inserts wish lexicographically *)
PROCEDURE InsertAlphabetically(wlp: WishListPtr; child, wish: String);

(* prepends (is called 'insertfront' for     *)
(* consistency with other insertion methods) *)
PROCEDURE InsertFront(wlp: WishListPtr; child, wish: String);

(* appends (following the naming convention as well)*)
PROCEDURE InsertBack(wlp: WishListPtr; child, wish: String);

(* inserts wish using the inserter procedure *)
FUNCTION WishListFromFile(path: String; inserter: WLInserter): WishListPtr;

(* prints all list contents to stdout *)
PROCEDURE WriteList(wlp: WishListPtr);

(* responsible for disposal (returns true if deletion was successful )*)
FUNCTION DisposeWishList(wlp: WishListPtr): BOOLEAN;


IMPLEMENTATION

CONST
  ANCHOR_COUNT = -1;
  ANCHOR_WISH = '';

(* private utility function *)
FUNCTION ValidChildDeclaration(child: String) : BOOLEAN;
VAR
  valid: BOOLEAN;
  colon: INTEGER;
BEGIN
  valid := false;
  colon := Pos(':', child);
  IF (colon <> 0) (* accounts for empty string *) AND
    (Copy(child, 1, colon-1) <> '')               AND
    (colon = Length(child)) (* last character*)   THEN
      valid := true;

  ValidChildDeclaration := valid;
END;

(* private utility function *)
FUNCTION CreateChildNode(name: STRING) : ChildNodePtr;
VAR cnp: ChildNodePtr;
BEGIN
  (* <name> is never an empty string *)
  New(cnp);
  cnp^.name := name;
  cnp^.next := nil;
  CreateChildNode := cnp;
END;

(* private utility function *)
PROCEDURE AppendChild(VAR clp: ChildListPtr; child: ChildNodePtr);
VAR child_node: ChildNodePtr;
BEGIN
  IF clp <> nil THEN BEGIN
    child_node := clp;
    WHILE child_node^.next <> nil DO BEGIN
      child_node := child_node^.next;
    END;
    child_node^.next := child;
  END ELSE clp := child;
END;

(* private utility function *)
(* creates wish list node on heap *)
FUNCTION  CreateWishNode(child, wish: String): WishNodePtr;
VAR wnp: WishNodePtr;
BEGIN
  wnp := nil;
  IF wish <> '' THEN BEGIN
    New(wnp);
    wnp^.n := 1;
    wnp^.children := CreateChildNode(child);
    wnp^.wish := wish;
    wnp^.next := wnp;
    wnp^.pred := wnp;
  END;
  CreateWishNode := wnp;
END;

FUNCTION CreateWishListAnchor : WishListPtr;
VAR wlp: WishListPtr;
BEGIN
  New(wlp);
  wlp^.n := ANCHOR_COUNT;
  wlp^.wish := ANCHOR_WISH;
  wlp^.children := nil;
  wlp^.next := wlp;
  wlp^.pred := wlp;
  CreateWishListAnchor := wlp;
END;

(* private utility function *)
PROCEDURE InsertAfter(after, to_insert: WishNodePtr);
BEGIN
  IF (after <> nil) AND (to_insert <> nil) THEN BEGIN
    (* link <to_insert> and the previous successor of <after> *)
    to_insert^.next := after^.next;
    to_insert^.next^.pred := to_insert;
    (* link <to_insert> and <after> *)
    to_insert^.pred := after;
    after^.next := to_insert;
  END;
END;

PROCEDURE InsertAlphabetically(wlp: WishListPtr; child, wish: String);
VAR node, after: WishNodePtr;
BEGIN
  node := nil;
  IF (wlp <> nil) AND (wish <> '') THEN BEGIN

    after := wlp^.next;
    WHILE (after <> wlp) AND (after^.wish < wish) DO
      (* after^.next is guaranteed not to be nil *)
      (* due to implementation of InsertAfter and CreateWishNode *)
      after := after^.next;

    IF (after = wlp) OR (after^.wish > wish) THEN BEGIN
      node := CreateWishNode(child, wish);
      InsertAfter(after^.pred, node);
    END ELSE IF after^.wish = wish THEN BEGIN
      after^.n += 1;
      AppendChild(after^.children, CreateChildNode(child));
    END;

  END;
END;

PROCEDURE InsertFront(wlp: WishListPtr; child, wish: String);
VAR node, buffer: WishNodePtr;
BEGIN
  node := nil;
  IF (wlp <> nil) AND (wish <> '') THEN BEGIN

    buffer := wlp^.next;
    WHILE (buffer <> wlp) AND (buffer^.wish <> wish) DO
      (* buffer^.next is guaranteed to not be nil *)
      (* due to implementation of InsertAfter and CreateWishNode *)
      buffer := buffer^.next;

    IF buffer = wlp THEN BEGIN
      node := CreateWishNode(child, wish);
      InsertAfter(wlp, node);
    END ELSE BEGIN (* wishes must have matched *)
      buffer^.n += 1;
      AppendChild(buffer^.children, CreateChildNode(child));
      (* link predecessor and successor of <buffer> *)
      buffer^.pred^.next := buffer^.next;
      buffer^.next^.pred := buffer^.pred;
      InsertAfter(wlp, buffer);
    END;

  END;
END;

PROCEDURE InsertBack(wlp: WishListPtr; child, wish: String);
VAR node, after: WishNodePtr;
BEGIN
  //WriteLn('INSERTER(wlp, ', child, ', ', wish, ');');
  node := nil;
  IF (wlp <> nil) AND (wish <> '') THEN BEGIN

    after := wlp^.next;
    WHILE (after <> wlp) AND (after^.wish <> wish) DO
      (* after^.next is guaranteed to not be nil *)
      (* due to implementation of InsertAfter and CreateWishNode *)
      after := after^.next;

    IF after = wlp THEN BEGIN
      node := CreateWishNode(child, wish);
      InsertAfter(wlp^.pred, node);
    END ELSE BEGIN (* wishes must have matched *)
      after^.n += 1;
      AppendChild(after^.children, CreateChildNode(child));
    END;

  END;
END;

FUNCTION WishListFromFile(path: String; inserter: WLInserter): WishListPtr;
VAR
  wish_file: TEXT;
  wish, current_child: String;
  wlp: WishListPtr;
BEGIN
  Assign(wish_file, path);
  Reset(wish_file);

  (* first child *)
  ReadLn(wish_file, current_child);

  (* wishlist must start with a child *)
  IF ValidChildDeclaration(current_child) THEN BEGIN
    wlp := CreateWishListAnchor;
    current_child := Copy(current_child, 1, Length(current_child)-1);

    REPEAT
      ReadLn(wish_file, wish);
      IF ValidChildDeclaration(wish) THEN BEGIN
        current_child := Copy(wish, 1, Length(wish)-1);
      END ELSE BEGIN
        inserter(wlp, current_child, wish);
      END;
    UNTIL Eof(wish_file);
  END;

  (* Cleanup *)
  Close(wish_file);

  WishListFromFile := wlp;
END;

PROCEDURE WriteList(wlp: WishListPtr);
VAR
  node: WishNodePtr;
  child: ChildNodePtr;
BEGIN
  IF wlp <> nil THEN BEGIN
    node := wlp^.next;
    WHILE node <> wlp DO BEGIN
      Write('(', node^.n,') ', node^.wish, ': ');
      child := node^.children;
      WHILE child <> nil DO BEGIN
        Write(child^.name);
        IF child^.next <> nil THEN Write(', ');
        child := child^.next;
      END;
      WriteLn;
      node := node^.next;
    END;
  END;
END;

FUNCTION DisposeWishList(wlp: WishListPtr): BOOLEAN;
VAR
  node: WishNodePtr;
  child, succ: ChildNodePtr;
BEGIN
  DisposeWishList := false;
  IF wlp <> nil THEN BEGIN (* list does exist *)

    IF wlp^.next = wlp THEN (* list is empty *)
      Dispose(wlp)
    ELSE BEGIN (* list is not empty *)
      node := wlp^.next;
      WHILE node <> wlp DO BEGIN
        (* delete singly linked children list *)
        child := node^.children;
        WHILE child <> nil DO BEGIN
          succ := child^.next;
          Dispose(child);
          child := succ;
        END;
        node := node^.next;
        (* delete node itself *)
        Dispose(node^.pred);
      END;
      Dispose(wlp);
    END; (* ! list is not empty *)
    DisposeWishList := true;

  END; (* ! list does exist *)
END;

BEGIN END.
