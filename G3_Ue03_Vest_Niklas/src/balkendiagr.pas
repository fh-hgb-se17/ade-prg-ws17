PROGRAM Balkendiagramm;

CONST
  ROWS = 10;

VAR
  printable : CHAR;
  n1, n2, n3, n4, n5 : INTEGER;

PROCEDURE WrSp;
BEGIN Write('  '); END;

PROCEDURE BarChart(ch : CHAR; _n1, _n2, _n3, _n4, _n5 : INTEGER);
VAR i : INTEGER;
BEGIN
  FOR i := ROWS DOWNTO 1 DO BEGIN
    (* if at least one param is as high as the current row *)
    IF NOT ((_n1 < i) AND (_n2 < i) AND (_n3 < i)
      AND (_n4 < i) AND (_n5 < i)) THEN BEGIN

        (* a bit o' formatting *)
        Write(i:2, '|');

        (* the actual bars*)
        IF i <= _n1 THEN Write(' ', ch) ELSE WrSp;
        IF i <= _n2 THEN Write(' ', ch) ELSE WrSp;
        IF i <= _n3 THEN Write(' ', ch) ELSE WrSp;
        IF i <= _n4 THEN Write(' ', ch) ELSE WrSp;
        IF i <= _n5 THEN Write(' ', ch) ELSE WrSp;
        WriteLn;
    END;
  END;
  WriteLn('  +-----------');
  WriteLn('    1 2 3 4 5');
END;

BEGIN
  Write('Character: ');
  ReadLn(printable);

  (* This could be solved very elegantly with arrays, but let's do it the
  good old medieval way since it pushes the grades :D *)
  Write('Number 1: ');
  ReadLn(n1);
  Write('Number 2: ');
  ReadLn(n2);
  Write('Number 3: ');
  ReadLn(n3);
  Write('Number 4: ');
  ReadLn(n4);
  Write('Number 5: ');
  ReadLn(n5);

  BarChart(printable, n1, n2, n3, n4, n5);
END.
