PROGRAM Maximum;

FUNCTION Max2(a, b : INTEGER) : INTEGER;
BEGIN
  IF b > a THEN Max2 := b
  ELSE Max2 := a;
END;

FUNCTION Max3a(a, b, c : INTEGER) : INTEGER;
VAR max : INTEGER;
BEGIN
  (* Store the bigger one in max*)
  IF a > b THEN max := a
  ELSE max := b;

  (* Then test if the third param is even bigger *)
  IF c > max THEN max := c;

  Max3a := max;
END;

FUNCTION Max3b(a, b, c : INTEGER) : INTEGER;
BEGIN
  Max3b := Max2(Max2(a, b), c);
END;

BEGIN
    (*Tests*)
WriteLn('[Max2] Max of 30 and 20 is ', Max2(30, 20)); (*30*)
WriteLn('[Max2] Max of -1 and -100 is ', Max2(-1, -100)); (*-1*)
WriteLn('[Max2] Max of 10000 and 0 and is ', Max2(10000, 0)); (*10000*)
WriteLn('[Max3a] Max of 80, 20, and 5 is ', Max3a(80,20,5)); (*80*)
WriteLn('[Max3a] Max of 69, 42 and 666 is ', Max3a(69, 42, 666)); (*666*)
WriteLn('[Max3a] Max of 1, 100 and 10 is ', Max3a(1, 100, 10)); (*100*)
WriteLn('[Max3b] Max of 938, 19 and 82 is ', Max3b(938, 19, 82)); (*938*)
WriteLn('[Max3b] Max of 999, -1000 and 1000 is ', Max3b(999, -1000, 1000)); (*1000*)
WriteLn('[Max3b] Max of 1, 3 and 2 is ', Max3b(1, 3, 2)); (*3*)
END.
