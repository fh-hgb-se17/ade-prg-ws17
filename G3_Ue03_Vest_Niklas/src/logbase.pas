PROGRAM AnyLogBase;

VAR num, base : INTEGER;

(* returns x^n ('x to the power of n') *)
FUNCTION Power(x, n : INTEGER) : INTEGER;
VAR i, final : INTEGER;
BEGIN
  final := x;
  FOR i := 2 TO n DO final *= x;
  Power:=final;

  IF n = 0 THEN Power:= 1;
END;

FUNCTION AnyIntLog(x, base : INTEGER) : INTEGER;
VAR n : INTEGER;
BEGIN
  (* defensive programming since the task doesn't state whether the input
  is ensured to be valid *)
  IF (x < 1) OR (base < 2) THEN BEGIN
    WriteLn('x must be greater than 0 and y must be greater than 1');
    AnyIntLog := -1;
  END ELSE BEGIN
    n := 0;
    (* returns last n before base^n causes an overflow *)
    WHILE (Power(base, n) <= x) AND (Power(base, n) >= 0) DO BEGIN
      AnyIntLog := n;
      n+=1;
    END;
  END;
END;

BEGIN
  Write('Number: ');
  ReadLn(num);
  Write('to Base: ');
  ReadLn(base);

  Write('The logarithm of ', num, ' to base ', base, ' is approximately ', AnyIntLog(num, base));
END.
