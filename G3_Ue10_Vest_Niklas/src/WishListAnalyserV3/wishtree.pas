UNIT WishTree;


INTERFACE

TYPE

  (* a bit more generic than usual *)
  StrTreeNodePtr = ^StrTreeNode;
  StrTreeNode = RECORD
    value: String;
    left, right: StrTreeNodePtr;
  END;
  StrTreePtr = StrTreeNodePtr;

  (* this would be way fancier using inheritance :c *)
  ChildTreeNodePtr = ^ChildTreeNode;
  ChildTreeNode = RECORD
    name: String;
    left, right: ChildTreeNodePtr;
    wishes: StrTreeNodePtr;
  END;
  ChildTreePtr = ChildTreeNodePtr;

(* inserts wish using the inserter procedure *)
FUNCTION ChildTreeFromFile(path: String): ChildTreePtr;

(* prints all list contents to stdout *)
PROCEDURE WriteChildTree(tp: ChildTreePtr);

(* responsible for disposal (returns true if deletion was successful )*)
PROCEDURE DisposeChildTree(tp: ChildTreePtr);


IMPLEMENTATION

(* private utility function *)
FUNCTION ValidChildDeclaration(child: String) : BOOLEAN;
VAR
  valid: BOOLEAN;
  colon: INTEGER;
BEGIN
  valid := false;
  colon := Pos(':', child);
  IF (colon <> 0) (* accounts for empty string *) AND
    (Copy(child, 1, colon-1) <> '')               AND
    (colon = Length(child)) (* last character*)   THEN
      valid := true;

  ValidChildDeclaration := valid;
END;

(* private utility function *)
FUNCTION CreateWishNode(str: STRING) : StrTreeNodePtr;
VAR tnp: StrTreeNodePtr;
BEGIN
  (* <str> is never an empty string *)
  New(tnp);
  tnp^.left := nil;
  tnp^.right := nil;
  tnp^.value := str;

  CreateWishNode := tnp;
END;

(* private utility function *)
FUNCTION CreateChildNode(name: STRING) : ChildTreePtr;
VAR tp: ChildTreePtr;
BEGIN
  (* <name> is never an empty string *)
  New(tp);
  tp^.left := nil;
  tp^.right := nil;
  tp^.wishes := nil;
  tp^.name := name;

  CreateChildNode := tp;
END;

(* private utility function *)
PROCEDURE AppendWish(VAR tree: StrTreePtr; node: StrTreeNodePtr);
BEGIN
  IF (tree <> nil) AND (node <> nil) THEN BEGIN

    IF tree^.value > node^.value THEN
      AppendWish(tree^.left, node)
    ELSE IF tree^.value < node^.value THEN
      AppendWish(tree^.right, node);

  END ELSE tree := node;
END;

(* private utility function *)
(* inheritance is my best friend in this scenario.. and i miss it dearly :c *)
PROCEDURE AppendChild(VAR tree: ChildTreePtr; node: ChildTreeNodePtr);
BEGIN
  IF (tree <> nil) AND (node <> nil) THEN BEGIN

    IF tree^.name > node^.name THEN
      AppendChild(tree^.left, node)
    ELSE IF tree^.name < node^.name THEN
      AppendChild(tree^.right, node);

  END ELSE tree := node;
END;

FUNCTION ChildTreeFromFile(path: String): ChildTreePtr;
VAR
  wish_file: TEXT;
  wish, current_child: String;
  tree: ChildTreePtr;
  cbuff: ChildTreeNodePtr;
  wbuff: StrTreeNodePtr;
BEGIN
  wbuff := nil;
  Assign(wish_file, path);
  Reset(wish_file);

  (* first child *)
  ReadLn(wish_file, current_child);

  (* wishlist must start with a child *)
  IF ValidChildDeclaration(current_child) THEN BEGIN
    current_child := Copy(current_child, 1, Length(current_child)-1);
    tree := CreateChildNode(current_child);
    cbuff := tree;

    REPEAT
      ReadLn(wish_file, wish);
      IF ValidChildDeclaration(wish) THEN BEGIN
        cbuff^.wishes := wbuff; (* add wishes to current child *)
        AppendChild(tree, cbuff); (* insert child into tree *)
        current_child := Copy(wish, 1, Length(wish)-1);
        cbuff := CreateChildNode(current_child); (* create a new child node *)
        wbuff := nil; (* reset wish tree *)
      END ELSE IF wish <> '' THEN BEGIN
        AppendWish(wbuff, CreateWishNode(wish)); (* add wish to current child's wish tree *)
      END;
    UNTIL Eof(wish_file);
    (* add last sub-tree *)
    cbuff^.wishes := wbuff;
    AppendChild(tree, cbuff);
  END;

  (* Cleanup *)
  Close(wish_file);

  ChildTreeFromFile := tree;
END;

(* private utility function *)
(* writes in-order*)
PROCEDURE WriteWishTree(tp: StrTreePtr);
BEGIN
  IF tp <> nil THEN BEGIN
    WriteWishTree(tp^.left);
    Write(tp^.value, ', ');
    WriteWishTree(tp^.right);
  END;
END;

(* writes in-order *)
PROCEDURE WriteChildTree(tp: ChildTreePtr);
BEGIN
  IF tp <> nil THEN BEGIN
    WriteChildTree(tp^.left);
    Write(tp^.name, ': ');
    WriteWishTree(tp^.wishes);
    WriteLn;
    WriteChildTree(tp^.right);
  END;
END;

(* private utility function *)
PROCEDURE DisposeWishTree(tp: StrTreePtr);
BEGIN
  IF tp <> nil THEN BEGIN
    DisposeWishTree(tp^.left);
    DisposeWishTree(tp^.right);
    Dispose(tp);
  END;
END;

PROCEDURE DisposeChildTree(tp: ChildTreePtr);
BEGIN
  IF tp <> nil THEN BEGIN
    DisposeChildTree(tp^.left);
    DisposeChildTree(tp^.right);
    DisposeWishTree(tp^.wishes);
    Dispose(tp);
  END;
END;

BEGIN END.
