PROGRAM WishListAnalyser;

USES Crt, WishTree;

VAR
  wishes: ChildTreePtr;
  file_path: String;

BEGIN
  file_path := 'Wishes.txt';
  wishes := ChildTreeFromFile(file_path);
  WriteChildTree(wishes);
  DisposeChildTree(wishes);
END.
