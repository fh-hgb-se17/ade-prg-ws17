PROGRAM BinarySearch;

CONST
  MAX = 10;

TYPE
  IntArray = ARRAY[1..MAX] OF INTEGER;

(* returns true if arr holds the element x in the *)
(* interval of [left;right], false otherwise *)
FUNCTION searchBinary(arr: IntArray; left, right: INTEGER; x: INTEGER) : BOOLEAN;
VAR mid: INTEGER;
BEGIN
  IF (left >= 1) AND (right <= High(arr)) THEN BEGIN

    IF left <= right THEN BEGIN
      mid := (left+right) DIV 2;

      IF x < arr[mid] THEN
        searchBinary := searchBinary(arr, left, mid-1, x)
      ELSE IF x > arr[mid] THEN
        searchBinary := searchBinary(arr, mid+1, right, x)
      ELSE (* x = arr[mid] *)
        searchBinary := true;
    END ELSE
      searchBinary := false;

  END ELSE
    searchBinary := false;
END;

VAR
  test_arr: IntArray;
  i: INTEGER;

BEGIN
  (* init array *)
  FOR i := Low(test_arr) TO High(test_arr) DO BEGIN
    test_arr[i] := i*i;
  END;

  (* test *)
  WriteLn('Array contains all the square numbers!');
  WriteLn('Array contains 1: ', searchBinary(test_arr, Low(test_arr), High(test_arr), 1));
  WriteLn('Array contains 3: ', searchBinary(test_arr, Low(test_arr), High(test_arr), 3));
  WriteLn('Array contains 9: ', searchBinary(test_arr, Low(test_arr), High(test_arr), 9));
  WriteLn('Array contains 26: ', searchBinary(test_arr, Low(test_arr), High(test_arr), 26));
  WriteLn('Array contains 49: ', searchBinary(test_arr, Low(test_arr), High(test_arr), 49));
  WriteLn('Array contains 100: ', searchBinary(test_arr, Low(test_arr), High(test_arr), 100));
END.
