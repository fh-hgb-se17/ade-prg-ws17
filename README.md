# SE Rulez!

This is a git repo for all digital exercises in ADE & PRG including, but not limited to, programming assignments that you can check whenever you are stuck to get an idea of how a particular problem could be solved. :)

If you happen to find a bug, don't hesitate to file an issue!