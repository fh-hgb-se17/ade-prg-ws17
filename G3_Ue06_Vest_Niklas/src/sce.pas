PROGRAM ShortCircuitEvaluation;
(*$B+*)

FUNCTION IsElement(a : ARRAY OF INTEGER; x : INTEGER) : BOOLEAN;
VAR
  i : INTEGER;
  b : BOOLEAN;
BEGIN
  i := 0;
  b := FALSE;

  WHILE (i <= High(a)) AND (NOT b) DO BEGIN
    IF a[i] = x THEN b := TRUE;
    Inc(i);
  END;

  IsElement := b;
END;

VAR
  arr : ARRAY[0..4] OF INTEGER;
  i : INTEGER;
BEGIN
  Randomize;
  Write('Generated Array: ');
  FOR i := 0 TO 4 DO BEGIN arr[i] := Random(11); Write(arr[i], ', '); END;
  WriteLn;
  WriteLn('4 is an element in the array: ', IsElement(arr, 4));
  WriteLn('10 is an element in the array: ', IsElement(arr, 10));
END.
