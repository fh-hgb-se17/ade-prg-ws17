PROGRAM StringOperations;

(*
  Usually a trim function removes space
  characters on the left and right of a
  string. However, since the specification
  asks for all spaces to be removed,
  simply completely annihilate that space
  promoting son of a string
*)
FUNCTION Trim(s : String) : String;
VAR sp : INTEGER;
BEGIN
  sp := Pos(' ', s);
  (* as long as there are space characters
  inside that besh *)
  WHILE sp <> 0 DO BEGIN
    Delete(s, sp, 1);
    sp := Pos(' ', s);
  END;

  Trim := s;
END;

(*
  Interface similar to Delete(s, i, n):
  Operated-On string first, followed by parameters
  for changing the string -> Consistency is KEY
*)
FUNCTION DeleteSubstring(s, substr : String) : String;
VAR substr_ind : INTEGER;
BEGIN
  substr_ind := Pos(substr, s);
  IF substr_ind <> 0 THEN
    Delete(s, substr_ind, Length(substr));

  DeleteSubstring := s;
END;


BEGIN
  WriteLn(Trim(' h e  y '));
  WriteLn(Trim(' wub  alu bad ubd   ub !  '));

  WriteLn(DeleteSubstring('Hello Whaddup m8 World!', 'Whaddup m8 '));
  WriteLn(DeleteSubstring('Whaddup Hello World! m8?', ' Hello World!'))
END.
