PROGRAM Roulette;

FUNCTION BenefitForLuckyNr(luckyNr, bet : INTEGER) : INTEGER;
BEGIN
  BenefitForLuckyNr := 0;
  IF (bet >= 1) AND (luckyNr > 0) AND (luckyNr <= 36) THEN BEGIN
    IF Random(37) = luckyNr THEN BenefitForLuckyNr := bet*36
    ELSE BenefitForLuckyNr := 0;
  END ELSE WriteLn('Invalid Bet!');
END;

FUNCTION BenefitForEvenNr(bet : INTEGER) : INTEGER;
VAR nr : INTEGER;
BEGIN
  BenefitForEvenNr := 0;
  IF bet >= 1 THEN BEGIN
    nr := Random(37);
    IF (nr MOD 2 = 0) AND (nr <> 0) THEN BenefitForEvenNr := bet*2
  END ELSE WriteLn('Invalid Bet!');
END;

PROCEDURE TestBenefitFunc(strategy : INTEGER);
CONST STRATEGY_NAMES : ARRAY[0..2] OF String = ('BenefitForLuckyNr','BenefitForEvenNr','Martingale strategy');
VAR pot, max, bet, wins, losses, curr : INTEGER;
BEGIN
  (* initialization *)
  pot := 1000;
  max := pot;
  bet := 1;
  wins := 0;
  losses := 0;

  (* actual betting *)
  WHILE (pot > 0) AND (bet > pot) DO BEGIN
    pot -= bet;

    IF strategy = 0 THEN curr := BenefitForLuckyNr(Random(37), bet)
    ELSE IF (strategy = 1) OR (strategy = 2) THEN curr := BenefitForEvenNr(bet);

    pot += curr;

    IF curr > 0 THEN BEGIN
      wins += 1;
      (* Martingale strategy, suggested by multiple websites *)
      IF strategy = 2 THEN bet := 1;
    END
    ELSE IF curr = 0 THEN BEGIN
      losses += 1;
      IF strategy = 2 THEN bet *= 2;
    END;

    IF pot > max THEN max := pot;
  END;

  (* Output *)
  WriteLn('Testrun using ', STRATEGY_NAMES[strategy], ': ');
  WriteLn('Games lost: ', losses);
  WriteLn('Games won: ', wins);
  WriteLn('Max. money: ', max);
  WriteLn;
END;

BEGIN
  Randomize;
  TestBenefitFunc(0);
  TestBenefitFunc(1);
  TestBenefitFunc(2);
END.
