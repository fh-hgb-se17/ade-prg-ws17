PROGRAM EncryptionChecker;

FUNCTION CouldHaveSameOrigin(s1, s2 : STRING) : BOOLEAN;
CONST
  UNSET = '0';
VAR
  check_array : ARRAY['A'..'Z'] OF CHAR;
  c : CHAR;
  i : INTEGER;
  chso : BOOLEAN;
BEGIN
  FOR c := 'A' TO High(check_array) DO check_array[c] := UNSET;
  chso := TRUE;

  IF Length(s1) <> Length(s2) THEN chso := FALSE
  ELSE BEGIN
    i := 1;
    WHILE (i <= Length(s1)) AND (chso) DO BEGIN
      (* if character is not set, set it *)
      IF check_array[s1[i]] = UNSET THEN BEGIN
        (* first check if there is the same value for another key *)
        c := Low(check_array);
        WHILE (c <= High(check_array)) AND (chso) DO BEGIN
          IF check_array[c] = s2[i] THEN
            chso := FALSE;
          Inc(c);
        END;
        (* set equivalent for this character if it's not a duplicate *)
        check_array[s1[i]] := s2[i];
      END

      (* else check if the one in the map is equal to the one in the second string*)
      ELSE IF check_array[s1[i]] <> s2[i] THEN chso := FALSE;

      i +=1;
    END;

  END;

  CouldHaveSameOrigin := chso;
END;

BEGIN
  WriteLn(CouldHaveSameOrigin('ABCADEF', 'SJJSKLM')); (* FALSE: Index 2 and 3 *)
  WriteLn(CouldHaveSameOrigin('DIREKTEZUORDNUNGIKOP', 'EKVCIBCQMLVEHMHTKILA')); (* TRUE *)
END.
