PROGRAM Merge;

PROCEDURE MergeArrays(a1, a2 : ARRAY OF INTEGER; VAR a3 : ARRAY OF INTEGER; VAR n3 : INTEGER);
VAR
  a1_ind, a2_ind, a3_ind : INTEGER;

BEGIN
  a1_ind := 0;
  a2_ind := 0;
  a3_ind := 0;

  (* if there's not enough room for all elements of a1 and a2, skip *)
  IF High(a1) + High(a2) >= High(a3) THEN
    a3_ind := -1
  ELSE BEGIN

    (* +1 because both a1 and a2 are 0-indexed *)
    WHILE a3_ind <= (High(a1) + High(a2) + 1) DO BEGIN

      (* if both arrays have uncopied values left *)
      IF (a1_ind <= High(a1)) AND (a2_ind <= High(a2)) THEN BEGIN
        (*decide which value to copy next*)
        IF (a1[a1_ind] < a2[a2_ind]) THEN BEGIN
          a3[a3_ind] := a1[a1_ind];
          a1_ind += 1;
        END
        ELSE BEGIN
          a3[a3_ind] := a2[a2_ind];
          a2_ind += 1;
        END; (* IF ELSE *)
      END ELSE BEGIN
        (* if one of the arrays is fully copied, simply add
        remaining values in the other array to target array*)
        IF a1_ind > High(a1) THEN BEGIN
          a3[a3_ind] := a2[a2_ind];
          a2_ind += 1;
        END ELSE BEGIN
          a3[a3_ind] := a1[a1_ind];
          a1_ind += 1;
        END;
      END; (* IF ELSE *)
      a3_ind += 1;
    END; (* WHILE *)

  END; (* IF target array big enough *)

  n3 := a3_ind;
END;

(* For testing purposes only; a3 is called by value
because we want a fresh empty array for every test *)
PROCEDURE Test(VAR a1, a2 : ARRAY OF INTEGER; a3 : ARRAY OF INTEGER);
VAR i, size : INTEGER;
BEGIN
  FOR i := Low(a1) TO High(a1) DO Write(a1[i], ', '); WriteLn;
  FOR i := Low(a2) TO High(a2) DO Write(a2[i], ', '); WriteLn;
  MergeArrays(a1, a2, a3, size);
  FOR i := Low(a3) TO (Low(a3) + (size-1)) DO Write(a3[i], ', '); WriteLn;
  WriteLn('Size: ', size);
END;

CONST
  arr1 : ARRAY[1..10] OF INTEGER = (1,1,2,5,6,8,8,9,13,19);
  arr2 : ARRAY[1..8] OF INTEGER = (2,3,4,6,7,7,8,9);
  arr3 : ARRAY[1..9] OF INTEGER = (1,2,4,5,6,6,7,8,8);
  arr4 : ARRAY[1..3] OF INTEGER = (0,5,7);

VAR
  merged : ARRAY[1..18] OF INTEGER;
  i : INTEGER;

BEGIN
  FOR i := Low(merged) TO High(merged) DO merged[i] := 0;
  Test(arr1, arr2, merged);
  Test(arr3, arr4, merged);
END.
