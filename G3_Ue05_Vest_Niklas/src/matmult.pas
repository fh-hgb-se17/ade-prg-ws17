PROGRAM MatrixMultiplication;

(* returns the onedimensional index for the matrix
indices [i, j] within a r*c matrix. If the dimension
parameters are invalid or the indices are out of bounds:
returns -1*)
FUNCTION IndexFor(r, c : INTEGER; i, j : INTEGER) : INTEGER;
BEGIN
  IF (i <= c) AND (i >= 1) AND
    (j <= r) AND (j >= 1) AND
    (r >= 1) AND (c >= 1) THEN
      IndexFor := (j-1) * c + i
  ELSE IndexFor := -1;
END;

PROCEDURE MatMult(r1, c1 : INTEGER; m1 : ARRAY OF REAL;
                  r2, c2 : INTEGER; m2 : ARRAY OF REAL;
                  VAR r3, c3 : INTEGER; VAR m3 : ARRAY OF REAL);
VAR
  row, col, calc_ind : INTEGER;
  scalar : REAL;
BEGIN
  (* condition for valid matrix multiplication:
  # columns of Matrix A = # rows of matrix B *)
  IF c1 = r2 THEN BEGIN

    (* set resulting dimensions *)
    r3 := r1;
    c3 := c2;

    FOR row := 1 TO r1 DO BEGIN
      FOR col := 1 TO r2 DO BEGIN
        scalar := 0;
        FOR calc_ind := 1 TO c2 DO BEGIN
          scalar += m1[IndexFor(r1, c1, calc_ind, row)-1] * m2[IndexFor(r2, c2, col, calc_ind)-1];
        END;
        m3[IndexFor(r3, c3, col, row)-1] := scalar;
      END;
    END;

  END
  ELSE BEGIN
    WriteLn('The supplied dimensions for matrices m1 and m2 were invalid.');
  END;
END;

CONST
  DIM_X_1 = 2;
  DIM_Y_1 = 3;
  DIM_X_2 = 3;
  DIM_Y_2 = 3;

  mat1 : ARRAY[1..DIM_X_1*DIM_Y_1] OF REAL =
    (1, 2, 1,
     1, 1, 1);

  mat2 : ARRAY[1..DIM_X_2*DIM_Y_2] OF REAL =
    (1, 1, 1,
     1, 1, 2,
     1, 3, 1);

VAR
  mat3 : ARRAY[1..DIM_X_1*DIM_Y_1] OF REAL;
  i, j, r, c : INTEGER;
BEGIN
  (* initialize *)
  r := 0;
  c := 0;
  FOR i := 1 TO High(mat3) DO mat3[i] := 0;

  (* Test *)
  MatMult(DIM_X_1, DIM_Y_1, mat1, DIM_X_2, DIM_Y_2, mat2, r, c, mat3);
  FOR i := 1 TO r DO BEGIN
    FOR j := 1 TO c DO
      Write(mat3[IndexFor(r,c,j,i)]:2:0, ', ');
    WriteLn;
  END;
END.
